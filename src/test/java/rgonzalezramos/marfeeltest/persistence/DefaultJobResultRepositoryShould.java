package rgonzalezramos.marfeeltest.persistence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import rgonzalezramos.marfeeltest.model.JobStatus;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

import static org.junit.Assert.*;

/**
 * Integrated test for the repository. If a non-embedded DB is used for
 * production, it'll be necessary to map to a test-specific context with
 * an in-memory DB.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:marfeel-context.xml")
public class DefaultJobResultRepositoryShould {
    @Autowired
    @Qualifier("repository")
    DefaultJobResultRepository sut;

    @Test
    public void saveAndRetrieveSaved() {
        JobResult saved = someEntity();
        
        sut.save(Arrays.asList(saved));
        
        // There's something in there
        Iterable<JobResult> all = sut.all();
        Iterator<JobResult> iterator = all.iterator();
        assertTrue(iterator.hasNext());

        // Retrieved matches saved
        JobResult retrieved = iterator.next();
        assertEquals(saved.getStatus(), retrieved.getStatus());
        assertEquals(saved.getAt(), retrieved.getAt());
        
        // And there's nothing else in there
        assertFalse(iterator.hasNext());
    }

	private JobResult someEntity() {
		JobResult jobResult = new JobResult();
        jobResult.setAt(new Date());
        jobResult.setStatus(JobStatus.ERROR);
		return jobResult;
	}
}