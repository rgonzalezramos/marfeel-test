package rgonzalezramos.marfeeltest.delivery;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import rgonzalezramos.marfeeltest.model.JobRequestDTO;

import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static rgonzalezramos.marfeeltest.utils.TestUtils.toJsonBytes;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:marfeel-context.xml")
public class MarfeelizableControllerShould {
    @Autowired
    WebApplicationContext wac;
    private MockMvc mvc;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void acceptAJsonArrayOfJobRequestsAndReturnAJsonArrayOfJobResponses() throws Exception {
        // This is little more than a smoke test for the controller
        String jobUrl = "c-and-a.com";
        byte[] requestBody = toJsonBytes(Collections.singletonList(JobRequestDTO.forUrl(jobUrl)));
        MockHttpServletRequestBuilder request = post("/marfeelizable").contentType(MediaType.APPLICATION_JSON_UTF8).content(requestBody);

        MvcResult result = mvc.perform(request).andReturn();
        mvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("[0].url").value(jobUrl));
    }

}
