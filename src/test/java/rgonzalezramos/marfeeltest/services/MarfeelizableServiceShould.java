package rgonzalezramos.marfeeltest.services;

import org.junit.Test;
import rgonzalezramos.marfeeltest.model.JobRequestDTO;
import rgonzalezramos.marfeeltest.model.JobResultDTO;
import rgonzalezramos.marfeeltest.model.JobStatus;
import rgonzalezramos.marfeeltest.utils.MarfeelizableServiceBuilder;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MarfeelizableServiceShould {
    private static final List<JobRequestDTO> SOME_JOBS = Arrays.asList(JobRequestDTO.forUrl("some url"),
            JobRequestDTO.forUrl("some other url"));

    @Test
    public void returnMarfeelizablesWhenUrlsMatch() {
        MarfeelizableService sut = new MarfeelizableServiceBuilder().withClientResponse("").withMatcher(true).build();
        List<JobResultDTO> results = sut.processJobs(SOME_JOBS);
        assertResults(SOME_JOBS.size(), JobStatus.MARFEELIZABLE, results);
    }

    @Test
    public void returnNotMarfeelizablesWhenUrlsMatch() {
        MarfeelizableService sut = new MarfeelizableServiceBuilder().withClientResponse("").withMatcher(false).build();
        List<JobResultDTO> results = sut.processJobs(SOME_JOBS);
        assertResults(SOME_JOBS.size(), JobStatus.NOT_MARFEELIZABLE, results);
    }

    @Test
    public void returnErrorsOnError() {
        MarfeelizableService sut = new MarfeelizableServiceBuilder().withClientException().build();
        List<JobResultDTO> results = sut.processJobs(SOME_JOBS);
        assertResults(SOME_JOBS.size(), JobStatus.ERROR, results);
    }

    private void assertResults(int size, JobStatus expectedStatus, List<JobResultDTO> results) {
        assertEquals(2, results.size());
        results.stream().forEach((result) -> assertEquals(expectedStatus, result.status));
    }

}
