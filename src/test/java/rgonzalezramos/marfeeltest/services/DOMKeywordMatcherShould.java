package rgonzalezramos.marfeeltest.services;

import org.junit.Test;
import rgonzalezramos.marfeeltest.services.DOMKeywordMatcher;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DOMKeywordMatcherShould {
    @Test
    public void matchWhenKeywordsArePresent() {
        DOMKeywordMatcher sut = new DOMKeywordMatcher(Arrays.asList("news"), "head > title");
        assertTrue(sut.match("<html><head><title>news</title></head></html>"));
    }

    @Test
    public void notMatchWhenKeywordsAreNotPresent() {
        DOMKeywordMatcher sut = new DOMKeywordMatcher(Arrays.asList("news"), "head > title");
        assertFalse(sut.match("<html><head><title>the keyword is not here</title></head></html>"));
    }

    @Test
    public void matchWhenOnlySomeOfTheKeywordsArePresent() {
        DOMKeywordMatcher sut = new DOMKeywordMatcher(Arrays.asList("news", "noticias"), "head > title");
        assertTrue(sut.match("<html><head><title>las noticias</title></head></html>"));
    }

    @Test
    public void notMatchWhenTheKeywordIsPartOfAnotherWord() {
        DOMKeywordMatcher sut = new DOMKeywordMatcher(Arrays.asList("news", "noticias"), "head > title");
        assertFalse(sut.match("<html><head><title>las nonoticias</title></head></html>"));
    }

    @Test
    public void notMatchWhenTheKeywordIsOutsideTheSelector() {
        DOMKeywordMatcher sut = new DOMKeywordMatcher(Arrays.asList("news", "noticias"), "head > title");
        assertFalse(sut.match("<html><head><title></title></head><body>I've got news in the body</body></html>"));
    }

    @Test
    public void notMatchWhenTheSelectorDoesntMatch() {
        DOMKeywordMatcher sut = new DOMKeywordMatcher(Arrays.asList("news", "noticias"), "head > title");
        assertFalse(sut.match("<html><head></head><body>No title but news!</body></html>"));
    }
}
