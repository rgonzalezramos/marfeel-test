package rgonzalezramos.marfeeltest.services;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class URLSanitizerShould {
    @Test
    public void addDefaultProtocolToURLsWithoutProtocol() {
        URLSanitizer sut = new URLSanitizer("https");
        assertEquals("https://c-and-a.com", sut.sanitize("c-and-a.com"));
    }

    @Test
    public void returnNullForNullUrls() {
        URLSanitizer sut = new URLSanitizer("https");
        assertNull(sut.sanitize(null));
    }

    @Test
    public void returnEmptyStringForEmptyUrls() {
        URLSanitizer sut = new URLSanitizer("https");
        assertEquals("", sut.sanitize(""));
    }

    @Test
    public void leaveURLsWithProtocolsAsTheyAre() {
        URLSanitizer sut = new URLSanitizer("https");
        String url = "http://terra.es";
        assertEquals(url, sut.sanitize(url));
    }

}