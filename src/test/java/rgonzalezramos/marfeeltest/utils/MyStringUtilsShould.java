package rgonzalezramos.marfeeltest.utils;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MyStringUtilsShould {

    @Test
    public void joinSeveralStringsProperly() {
        String join = MyStringUtils.join(Arrays.asList("one", "two", "three"), ";");
        assertEquals(join, "one;two;three");
    }

    @Test
    public void returnAnEmptyStringWhenRequestedToJoinAnEmptyList() {
        String join = MyStringUtils.join(Collections.EMPTY_LIST, ";");
        assertEquals(join, "");
    }

    @Test
    public void returnTheSameStringWhenRequestedToJoinASingleElementList() {
        String join = MyStringUtils.join(Collections.singletonList("foo"), ";");
        assertEquals(join, "foo");
    }

    @Test
    public void parseKeywords() {
        List<String> keywords = MyStringUtils.parseKeywords("noticias,   news");
        assertEquals(2, keywords.size());
        assertEquals("noticias", keywords.get(0));
        assertEquals("news", keywords.get(1));
    }
}
