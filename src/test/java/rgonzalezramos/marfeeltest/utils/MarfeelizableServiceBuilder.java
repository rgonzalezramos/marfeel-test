package rgonzalezramos.marfeeltest.utils;

import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.web.client.RestTemplate;
import rgonzalezramos.marfeeltest.persistence.JobResult;
import rgonzalezramos.marfeeltest.persistence.JobResultRepository;
import rgonzalezramos.marfeeltest.persistence.DefaultJobResultRepository;
import rgonzalezramos.marfeeltest.services.DOMKeywordMatcher;
import rgonzalezramos.marfeeltest.services.MarfeelizableService;
import rgonzalezramos.marfeeltest.services.URLSanitizer;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * A builder to ease the creation of MarfeelizableService test subjects
 */
public class MarfeelizableServiceBuilder {
    private SimpleAsyncTaskExecutor executor = new SimpleAsyncTaskExecutor();
    private RestTemplate client = null;
    private DOMKeywordMatcher matcher = null;
    private URLSanitizer sanitizer = new URLSanitizer("http");
    private DefaultJobResultRepository repository = mock(DefaultJobResultRepository.class);

    public MarfeelizableService build() {
        return new MarfeelizableService(executor, matcher, client, sanitizer, repository);
    }

    public MarfeelizableServiceBuilder withClientResponse(String response) {
        client = mockRestTemplate(response);
        return this;
    }

    public MarfeelizableServiceBuilder withMatcher(boolean response) {
        matcher = mockMatcher(response);
        return this;
    }

    public MarfeelizableServiceBuilder withClientException() {
        client = mockErrorRestTemplate();
        return this;
    }

    private DOMKeywordMatcher mockMatcher(boolean retVal) {
        DOMKeywordMatcher mock = mock(DOMKeywordMatcher.class);
        when(mock.match(anyString())).thenReturn(retVal);
        return mock;
    }

    private RestTemplate mockRestTemplate(String retVal) {
        RestTemplate mock = mock(RestTemplate.class);
        when(mock.getForObject(anyString(), any())).thenReturn(retVal);
        return mock;
    }

    private RestTemplate mockErrorRestTemplate() {
        RestTemplate mock = mock(RestTemplate.class);
        when(mock.getForObject(anyString(), any())).thenThrow(new RuntimeException());
        return mock;
    }
}
