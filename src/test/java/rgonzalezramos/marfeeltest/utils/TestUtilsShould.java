package rgonzalezramos.marfeeltest.utils;

import org.junit.Test;

import java.io.UnsupportedEncodingException;

import static org.junit.Assert.assertEquals;

public class TestUtilsShould {

    @Test
    public void convertToJSON() throws UnsupportedEncodingException {
        // Smoke test
        byte[] bytes = TestUtils.toJsonBytes(3);
        assertEquals("3", toString(bytes));
    }

    private String toString(byte[] bytes) throws UnsupportedEncodingException {
        return new String(bytes, "utf-8");
    }

}
