package rgonzalezramos.marfeeltest.services;

import rgonzalezramos.marfeeltest.utils.MyStringUtils;

/**
 * Adds a protocol to protocol-less urls
 */
public class URLSanitizer {
    public static final String HAS_PROTOCOL_REGEX = "^[a-zA-Z]*://.*$";
    private String defaultProtocol;

    public URLSanitizer(String defaultProtocol) {
        this.defaultProtocol = defaultProtocol;
    }

    public String sanitize(String url) {
        if (MyStringUtils.isNotEmpty(url)) {
            if (isMissingProtocol(url)) {
                return defaultProtocol + "://" + url;
            }
        }
        return url;
    }

    private boolean isMissingProtocol(String urlString) {
        return !urlString.matches(HAS_PROTOCOL_REGEX);
    }
}
