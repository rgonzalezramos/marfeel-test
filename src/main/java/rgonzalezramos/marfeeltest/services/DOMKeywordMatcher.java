package rgonzalezramos.marfeeltest.services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import rgonzalezramos.marfeeltest.utils.MyStringUtils;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * For a list of keywords and a CSS selector, determines if the selector
 * contains any of the keywords. Uses JSoup to extract the text from the
 * html tags matching the selector.
 */
public class DOMKeywordMatcher {
    private String selector;
    private Pattern pattern;

    public DOMKeywordMatcher(List<String> keywords, String selector) {
        // Create regular expression that matches all the keywords
        String regex = String.format("\\b(%s)\\b", MyStringUtils.join(keywords, "|"));
        // Cache the regular expression as a pattern
        this.pattern = Pattern.compile(regex);
        this.selector = selector;
    }

    public boolean match(String html) {
        String content = extractTextFromMatchingTags(html);
        return pattern.matcher(content).find();
    }

    private String extractTextFromMatchingTags(String html) {
        Document root = Jsoup.parse(html);
        Elements tagsMatchingSelector = root.select(selector);
        return tagsMatchingSelector.stream().map((tag) -> tag.text()).collect(Collectors.joining("."));
    }
}
