package rgonzalezramos.marfeeltest.services.errors;

import java.util.concurrent.RejectedExecutionException;

public class PoolIsFullException extends RuntimeException {
    public PoolIsFullException(RejectedExecutionException e) {
        super(e);
    }
}
