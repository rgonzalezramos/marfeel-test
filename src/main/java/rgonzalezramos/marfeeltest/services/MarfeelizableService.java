package rgonzalezramos.marfeeltest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import rgonzalezramos.marfeeltest.model.JobRequestDTO;
import rgonzalezramos.marfeeltest.model.JobResultDTO;
import rgonzalezramos.marfeeltest.persistence.JobResult;
import rgonzalezramos.marfeeltest.persistence.DefaultJobResultRepository;
import rgonzalezramos.marfeeltest.services.errors.PoolIsFullException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.stream.Collectors;

/**
 * Processes batches of urls in parallel, persisting and returning the associated results
 */
@Service
public class MarfeelizableService {
    private AsyncTaskExecutor executor;
    private DOMKeywordMatcher matcher;
    private RestTemplate client;
    private URLSanitizer sanitizer;
    private DefaultJobResultRepository repository;

    @Autowired
    public MarfeelizableService(AsyncTaskExecutor executor, DOMKeywordMatcher matcher, RestTemplate client, URLSanitizer sanitizer, DefaultJobResultRepository repository) {
        this.executor = executor;
        this.matcher = matcher;
        this.client = client;
        this.sanitizer = sanitizer;
        this.repository = repository;
    }

    public List<JobResultDTO> processJobs(List<JobRequestDTO> jobs) throws PoolIsFullException {
        List<Future<JobResultDTO>> futures = submitJobs(jobs);
        List<JobResultDTO> results = retrieveResults(futures);
        persist(results);
        return results;
    }

    public List<JobResultDTO> retrievePastJobs() {
        ArrayList<JobResultDTO> dtos = new ArrayList<>();
        Iterable<JobResult> entities = repository.all();
        for(JobResult entity : entities) {
            dtos.add(JobResultDTO.create(entity.getUrl(), entity.getStatus(), entity.getDetails(), entity.getAt()));
        }
        return dtos;
    }

    /**
     * Send partial jobs to the executor
     */
    private List<Future<JobResultDTO>> submitJobs(List<JobRequestDTO> jobs) throws PoolIsFullException {
        List<Future<JobResultDTO>> futures = new ArrayList<>(jobs.size());
        try {
            for (JobRequestDTO job : jobs) futures.add(submitJob(job));
        } catch (RejectedExecutionException e) {
            // Abort already submitted jobs
            abortAll(futures);
            throw new PoolIsFullException(e);
        }
        return futures;
    }

    private void abortAll(List<Future<JobResultDTO>> futures) {
        futures.forEach((future) -> future.cancel(true));
    }

    /**
     * Retrieve the results of the jobs submitted for execution
     */
    private List<JobResultDTO> retrieveResults(List<Future<JobResultDTO>> futures) {
        List<JobResultDTO> results = new ArrayList<>(futures.size());
        for (Future<JobResultDTO> future : futures) {
            try {
                results.add(future.get());
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }
        }
        return results;
    }

    private Future<JobResultDTO> submitJob(JobRequestDTO job) {
        return executor.submit(() -> processSingleJob(job));
    }

    private void persist(List<JobResultDTO> dtos) {
        // Map DTOs to entities
        List<JobResult> entities = dtos.stream()
                .map((dto) -> new JobResult(dto.url, dto.status, dto.at, dto.details))
                .collect(Collectors.toList());
        repository.save(entities);
    }

    private JobResultDTO processSingleJob(JobRequestDTO job) {
        try {
            String html = downloadContent(job.url);
            if (matcher.match(html)) {
                return JobResultDTO.marfeelizable(job.url);
            }
            return JobResultDTO.notMarfeelizable(job.url);
        } catch (Exception e) {
            return JobResultDTO.error(job.url, e);
        }
    }

    private String downloadContent(String url) {
        String saneUrl = sanitizer.sanitize(url);
        return client.getForObject(saneUrl, String.class);
    }
}
