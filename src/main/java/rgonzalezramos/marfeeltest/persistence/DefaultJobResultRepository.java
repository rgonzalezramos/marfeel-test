package rgonzalezramos.marfeeltest.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
@Transactional
public class DefaultJobResultRepository {
    @Autowired
    JobResultRepository repository;

    public void save(List<JobResult> jobs) {
        repository.save(jobs);
    }

    public Iterable<JobResult> all() {
        return repository.findAll();
    }
}
