package rgonzalezramos.marfeeltest.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface JobResultRepository extends JpaRepository<JobResult, Long> {
}
