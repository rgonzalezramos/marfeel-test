package rgonzalezramos.marfeeltest.persistence;

import rgonzalezramos.marfeeltest.model.JobStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="job_result")
public class JobResult {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String url;
    private JobStatus status;
    @Temporal(TemporalType.TIMESTAMP)
    private Date at;
    private String details;

    public JobResult() {
    }

    public JobResult(String url, JobStatus status, Date at, String details) {
        this.url = url;
        this.status = status;
        this.at = at;
        this.details = details;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public Date getAt() {
        return at;
    }

    public void setAt(Date at) {
        this.at = at;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return "JobResult{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", status=" + status +
                ", at=" + at +
                ", details='" + details + '\'' +
                '}';
    }
}
