package rgonzalezramos.marfeeltest.model;

import java.util.Date;

public class JobResultDTO {
    public String url;
    public JobStatus status;
    public Date at;
    public String details;

    public static JobResultDTO create(String url, JobStatus status) {
        return create(url, status, null, new Date());
    }

    public static JobResultDTO marfeelizable(String url) {
        return create(url, JobStatus.MARFEELIZABLE);
    }

    public static JobResultDTO notMarfeelizable(String url) {
        return create(url, JobStatus.NOT_MARFEELIZABLE);
    }

    public static JobResultDTO error(String url, Exception e) {
        return create(url, JobStatus.ERROR, e.getMessage(), new Date());
    }

    public static JobResultDTO create(String url, JobStatus status, String details, Date date) {
        JobResultDTO dto = new JobResultDTO();
        dto.url = url;
        dto.status = status;
        dto.details = details;
        dto.at = new Date();
        return dto;
    }
}
