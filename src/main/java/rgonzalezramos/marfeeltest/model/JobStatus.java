package rgonzalezramos.marfeeltest.model;

public enum JobStatus {
    MARFEELIZABLE, NOT_MARFEELIZABLE, ERROR
}
