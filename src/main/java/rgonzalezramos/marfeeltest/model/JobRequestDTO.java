package rgonzalezramos.marfeeltest.model;

public class JobRequestDTO {
    public String url;

    public static JobRequestDTO forUrl(String url) {
        JobRequestDTO dto = new JobRequestDTO();
        dto.url = url;
        return dto;
    }
}
