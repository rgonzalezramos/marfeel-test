package rgonzalezramos.marfeeltest.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Since apache-commons is not in the list of allowed libraries...
 */
public class MyStringUtils {

    /**
     * Joins a list of words using the given separator. This can be
     * easily emulated with map.stream() and Collectors.joining
     */
    public static String join(List<String> words, String separator) {
        StringBuilder sb = new StringBuilder();

        // Concatenate word + separator
        for (String word : words) {
            sb.append(word);
            sb.append(separator);
        }

        // Trim the last separator if it's there
        sb.setLength(Math.max(sb.length() - 1, 0));

        return sb.toString();
    }

    public static boolean isNotEmpty(String string) {
        return string != null && !string.isEmpty();
    }

    /**
     * Utility method to parse the properties file
     */
    public static List<String> parseKeywords(String string) {
        return Arrays.asList(string.split(",")).stream().map(String::trim).collect(Collectors.toList());
    }
}
