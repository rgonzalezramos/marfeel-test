package rgonzalezramos.marfeeltest.delivery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import rgonzalezramos.marfeeltest.model.JobRequestDTO;
import rgonzalezramos.marfeeltest.model.JobResultDTO;
import rgonzalezramos.marfeeltest.services.MarfeelizableService;
import rgonzalezramos.marfeeltest.services.errors.PoolIsFullException;

import java.util.List;
import java.util.concurrent.Callable;

@RestController
@RequestMapping("/marfeelizable")
public class MarfeelizableController {
    private MarfeelizableService service;

    @ResponseStatus(code = HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler(PoolIsFullException.class)
    public void handler() {
    }

    @Autowired
    public MarfeelizableController(MarfeelizableService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    public Callable<List<JobResultDTO>> marfeelizable(@RequestBody List<JobRequestDTO> requests) {
        // Return callable to take advantage of AsyncServlet
        return () -> service.processJobs(requests);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<JobResultDTO> all() {
        return service.retrievePastJobs();
    }
}
