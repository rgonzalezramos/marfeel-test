DROP TABLE job_result;

CREATE TABLE job_result (
    id BIGINT AUTO_INCREMENT,
    url VARCHAR(1024),
    at TIMESTAMP,
    details CLOB,
    status VARCHAR(32)
);